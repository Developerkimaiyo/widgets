import 'package:flutter/material.dart';

class GridItem extends StatelessWidget {
  final String title;
  final Color color;

  GridItem(this.title, this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(15),
      child: Center(child: Text(title,style: TextStyle(    fontSize: 14,color: Colors.white.withOpacity(0.8),fontWeight: FontWeight.bold),)),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            color.withOpacity(0.7),
            color,
          ],
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
        ),
        borderRadius: BorderRadius.circular(10),
      ),
    );
  }
}