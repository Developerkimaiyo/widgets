import 'package:flutter/material.dart';

import './model/data.dart';

const CATEGORIES = const [
  Category(
    id: '1',
    title: 'Flutter - Mobile',
    color: Colors.red,
  ),
  Category(
    id: '2',
    title: 'Vue - Frontend',
    color: Colors.orange,
  ),
  Category(
    id: '3',
    title: 'adonis - backend',
    color: Colors.amber,
  ),
  Category(
    id: '4',
    title: 'quarkus - backend',
    color: Colors.blue,
  ),
  Category(
    id: '5',
    title: 'go - backend',
    color: Colors.green,
  ),
];