import 'package:flutter/material.dart';

  class FormPage extends StatefulWidget {
  @override
  _FormPageState createState() => _FormPageState();
  }

  class _FormPageState extends State<FormPage> {
  String _chosenValue;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Card(
            elevation: 5,
            
            child: SingleChildScrollView(
              child: Container(
                
                padding: const EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,

                  children: <Widget>[

                     TextFormField(
                      decoration: new InputDecoration(
                        labelText: "Enter Name",
                        fillColor: Colors.white,
                      ),
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                          fontFamily: "Poppins",
                          color: Colors.black,
                          fontSize: 16,
                          fontWeight: FontWeight.w600
                      ),
                    ),
                      SizedBox(height: 20),
                      DropdownButton<String>(
                        isExpanded: true,
                        value: _chosenValue,
                        style: TextStyle(
                            fontFamily: "Poppins",
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                        underline: DropdownButtonHideUnderline(child: Container()),
                        items: <String>[
                          'Flutter',
                          'Adonis',
                          'GoLang',
                          'Quakers',
                          'Vue',
                          'MYSQL',
                        ].map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                        hint: Text(
                          "Please choose a language",
                          style: TextStyle(
                              fontFamily: "Poppins",
                              color: Colors.black,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        onChanged: (String value) {
                          setState(() {
                            _chosenValue = value;
                          });
                        },

                    ),
                    SizedBox(height: 10),
                    TextButton.icon(
                        label: Text('View'),
                        icon: Icon(Icons.remove_red_eye),

                        onPressed: () {
                          Navigator.pushNamed(context, '/GridPage');
                        }
                    )

                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }


}