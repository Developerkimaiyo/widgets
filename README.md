# Flutter: Widgets

This is a simple application with a splash screen, a page with a simple form and one with grids layout.

### Version: 1.0.0


| Image 1    | Image 2     | Image 3     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619561004/Widgets/3_ortuw8.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619561003/Widgets/1_n9ucw3.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1619561003/Widgets/2_r7mrdl.png" width="250"> |


### Usage

```js
git clone https://gitlab.com/Developerkimaiyo/widgets.git

```


### Support

Reach out to me at one of the following places!

- Twitter at <a href="http://twitter.com/maxxmalakwen" target="_blank">`@maxxmalakwen`</a>

Let me know if you have any questions. Email me At maxwell@sendyit.com or developerkimaiyo@gmail.com



---

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2021 © <a href="https://github.com/Developer-Kimaiyo" target="_blank">Maxwell Kimaiyo</a>.